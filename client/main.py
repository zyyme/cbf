import kivy

from kivy.app import App
from kivy.uix.label import Label
from kivy.core.window import Window
from kivy.uix.widget import Widget
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.screenmanager import Screen, ScreenManager
from kivy.config import Config
from kivy.properties import StringProperty
from kivy.clock import Clock
from kivy.uix.popup import Popup 
import pyperclip
import json
import requests

Window.size = (1024, 600)

kivy.require("1.10.1")

Window.clearcolor = (51/255,49/255,59/255, 1)

login = ''
password = ''

task = str()

class ScreenManagement(ScreenManager):
    pass


class LoginMenu(Screen):
    login = StringProperty("")
    password = StringProperty("")


    def proceed(self):
        print("Click!")
        r = s.post("http://139.59.156.71:5000/api/login", 
        json={"login":self.ids.login.text, "password":self.ids.password.text})
        if r.status_code == 200:
            self.login = self.ids.login.text
            self.password = self.ids.password.text
            self.parent.ids.menu.login = self.login
            login  = self.login
            password = self.password
            print(login, password)
            self.parent.current="main_menu"
        else:
            lbl = Label(text="Wrong User or Pass", font_size="26pt", color=(0.8,0.2,0.2,1), pos_hint={"top":0.7})
            self.add_widget(lbl)

class MainMenu(Screen):
    login = StringProperty("")
    
    def pinger(self,dt):
        
        r = s.post("http://139.59.156.71:5000/api/ping_room")
        print(r.status_code)
        if r.status_code != 200 and self.manager.current == "created_room":
            print("pinging")
            return True
        else:
            print("exiting")
            if r.status_code ==200:
                task = json.loads(r.text)["task_description"]
                self.manager.ids.code_screen.task = task
                self.manager.current = "code_screen"
            if self.manager.current == "main_menu":
                r = s.post("http://139.59.156.71:5000/api/close_room")
            return False
    

    def join(self):
        self.parent.current="rooms_list"
    def create(self):
        r = s.post("http://139.59.156.71:5000/api/create_room")
        Clock.schedule_interval(self.pinger, 1)
        json_data = json.loads(r.text)
        self.parent.ids.room.id = json_data["room_id"]
        self.parent.current = "created_room"

class CodeScreen(Screen):
    task = StringProperty("")
    def logout(self):
        self.parent.current = "login_menu"

    def submit(self):
        i = 1
        f = open("program.py", 'w')
        for line in str(self.ids.code.text).splitlines():
            f.write(line + "\n")
            i+=1
        f.close()   

        r = s.post("http://139.59.156.71:5000/api/submit_code", files = {"code": open("program.py", "rb")})
        if r.status_code == 200:
            json_data = json.loads(r.text)
            self.manager.ids.finish_screen.result = json_data["result"] 
            self.parent.current = "finish_screen"
        else:
            json_data = json.loads(r.text)
            print(json_data["error"])
            popup = Popup(title="Error", content = Label(text = json_data["error"]), size_hint=(None,None), size=(400,400))
            popup.open()


class RoomsList(Screen):
    
    def join_room(self):
        r = s.post("http://139.59.156.71:5000/api/join_room",
        json={"room_id": self.ids.enter.text})
        if r.status_code == 200:
            json_data = json.loads(r.text)
            task = json_data["task_description"]
            self.manager.ids.code_screen.task = task
            self.parent.current = "code_screen"
 
    def go_back(self):
        self.parent.current = "main_menu"
    
class Room(Screen):
    id = StringProperty("")


    def copy_id(self):
        pyperclip.copy(self.id)


    def go_back(self):
        self.parent.current = "main_menu"

class FinishScreen(Screen):
    result = StringProperty("")
    
    def go_back(self):
        if self.result == "Loser":
            r = s.post("http://139.59.156.71:5000/api/close_room")
            self.parent.current = "main_menu"
        else: 
            self.parent.current = "main_menu"
        
        self.result = ""

class CodeApp(App):
    title = "CodeBattle"
    def build(self):
        return ScreenManagement()

if __name__ == "__main__":
    s = requests.session()

    CodeApp().run()