teams_score = [0,0]

for  i in range(4):
    scores = [int(i) for i in str(input()).split(" ")]
    teams_score[0]+=scores[0]
    teams_score[1]+=scores[1]

if teams_score[0]>teams_score[1]:
    print("1")
elif teams_score[1]>teams_score[0]:
    print("2")
else:
    print("DRAW")
